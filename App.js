/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TextInput,
  Button,
  Alert
} from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reloadsas,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reloadsasa,\n" +
    "Shake or press menu button for dev menu"
});

class Greeting extends Component {
  render() {
    return (
      <Text style={this.props.style}>
        Hola {this.props.name}
        !! ✌
      </Text>
    );
  }
}

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { text: "" };
  }
  render() {
    let pic = {
      uri:
        "https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg"
    };
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Wacha el React Native power!!!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={(styles.instructions, styles.red)}>{instructions}</Text>

        <Image source={pic} style={{ width: 193, height: 110 }} />

        <Greeting name="Alvaro Mashiro" />
        <Greeting name="PoncheMonse🎶" />
        <Greeting name="Pablito!!" />
        <Greeting name="Holo" />
        <Greeting name="❤" />
        <ScrollView
          style={{
            flex: 1,
            height:50,
            // flexDirection: "row",
            backgroundColor: "powderblue"
          }}
        >
          <Greeting name="asdsa❤" />
          <Image source={pic} style={{ width: 193, height: 110 }} />
          <TextInput
            placeholder="asdAS"
            onChangeText={text => this.setState({ text })}
          />
          <Text style={{ padding: 10, fontSize: 42 }}>
            {this.state.text
              .split(" ")
              .map(word => word && "❤")
              .join(" ")}
          </Text>
          <Button
            color="#841584"
            onPress={() => {
              Alert.alert("You tapped the button!");
            }}
            title="Press Me"
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  red: {
    color: "red"
  }
});
